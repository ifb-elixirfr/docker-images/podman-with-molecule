FROM docker.io/python:3.8-bullseye

ENV ANSIBLE_VERSION='4.6.0'


RUN apt-get update --yes && apt-get upgrade --yes

RUN apt-get install --yes podman

RUN pip install ansible==${ANSIBLE_VERSION} ansible-lint molecule molecule-podman
